package sample.mybatis.mapper;

import sample.mybatis.domain.TranslateValueDataListReq;
import sample.mybatis.domain.User;

import java.util.List;
import java.util.Map;

/**
 * User的Mapper，用于Mybatis
 *
 * @author 小翼
 * @version 1.0.0
 */
public interface UserMapper {

	User selectUser(String username);

	List<Map<String, Object>> listStandardData(TranslateValueDataListReq req);
}

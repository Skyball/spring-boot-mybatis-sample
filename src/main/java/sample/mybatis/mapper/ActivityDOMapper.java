package sample.mybatis.mapper;

import com.dt.activity.center.bo.activity.ActivityBO;
import com.dt.activity.center.bo.activity.ActivityCountBO;
import com.dt.activity.center.bo.activity.ActivityPageBO;
import com.dt.activity.center.dto.activity.*;
import com.dt.activity.center.bo.activity.ActivityPlanPageBO;
import com.dt.activity.center.dto.theme.ThemeActivityPageDTO;
import com.dt.activity.center.feign.server.dto.ActivityCountAndLastResultDTO;
import com.dt.activity.center.feign.server.dto.ActivityCountQueryFeignDTO;
import com.dt.activity.center.feign.server.dto.SchoolUserStatisticsDTO;
import com.dt.activity.center.model.*;
import com.dt.activity.center.web.model.activity.ActivityPublishAllManagementListVO;
import com.dt.activity.center.web.model.activity.ActivityPublishPageVO;
import com.dt.activity.center.web.model.activity.group.ActivityPublishGroupUserCount;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 活动表 (Activity)表数据库访问层
 *
 * @author zhangzy
 * @since 2021-06-24 12:20:17
 */
public interface ActivityDOMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return
     */
    ActivityDO selectActivityById(Long id);

    /**
     * 查询指定行数据
     *
     */
    List<ActivityDO> selectListByPage(ActivityPageDTO activityPageDTO);

    /**
     * 选择性新增数据
     *
     * @param activity 实例对象
     */
    void insertSelective(ActivityDO activity);

    /**
     * 修改数据
     *
     * @param activity 实例对象
     */
    void updateByPrimaryKeySelective(ActivityDO activity);

    void updateByPrimaryKeyListSelective(ActivityPublishDTO activityPublishDTO);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     */
    void deleteByPrimaryKey(Long id);

    /***
     * @Description:查询活动访问码是否已使用
     * @Author: zhangzy
     * @Date: 2021/6/25 9:56
     * @param number
     *               {@link Integer}
     **/
    Integer countByActivityCode(Integer number);

    /***
     * @Description:根据条件查询活动详情
     * @Author: zhangzy
     * @Date: 2021/6/25 10:34
     * @param activityDetailDTO
     *                          {@link ActivityDO}
     **/
    ActivityDO selectActivityDetail(ActivityDetailDTO activityDetailDTO);

    /***
     * @Description:根据条件查询活动条数
     * @Author: zhangzy
     * @Date: 2021/6/28 11:47
     * @param activityPageDTO
     *                        {@link Integer}
     **/
    Integer countByActivityPageDTO(ActivityPageDTO activityPageDTO);

    /***
     * @Description:根据条件查询活动分页对象
     * @Author: zhangzy
     * @Date: 2021/6/28 12:21
     * @param activityPageDTO
     *                        {@link List< ActivityPageBO>}
     **/
    List<ActivityPageDO> selectActivityPageByActivityPageDTO(ActivityPageDTO activityPageDTO);

    /***
     * @Description:更新活动参与人数/活动浏览次数/热度
     * @Author: zhangzy
     * @Date: 2021/7/8 10:38
     * @param activityHotUpdateDTO
     *
     **/
    void updateCountAndHot(ActivityHotUpdateDTO activityHotUpdateDTO);

    /***
     * @Description:双师业务根据条件查询活动条数
     * @Author: zhangzy
     * @Date: 2021/7/29 9:51
     * @param activityPageDTO
     *                        {@link Integer}
     **/
    Integer countDoubleTeacherByActivityPageDTO(ActivityPageDTO activityPageDTO);

    /***
     * @Description:双师业务根据条件查询活动分页对象
     * @Author: zhangzy
     * @Date: 2021/7/29 12:19
     * @param activityPageDTO
     *                        {@link List< ActivityPageDO>}
     **/
    List<ActivityPageDO> selectDoubleTeacherActivityPageByActivityPageDTO(ActivityPageDTO activityPageDTO);

    /***
     * @Description:获取该活动的模板code
     * @Author: zhangzy
     * @Date: 2021/8/3 10:20
     * @param activityId
     *                   {@link Integer}
     **/
    Integer getTemplateCodeByActivityId(Long activityId);

    List<ActivityPageDO> listByActivityIds(@Param("ids") List<Long> ids);

    Integer countThemeActivityByPage(ThemeActivityPageDTO themeActivityPageDTO);

    List<ThemeActivityDOEx> listThemeActivityByPage(ThemeActivityPageDTO themeActivityPageDTO);

    /**
     * 后台管理页面统计
     *
     * @param dto
     * @return
     */
    Integer operateAllPageCount(ActivityPublishPageDTO dto);

    /**
     * 后台管理页面列表
     *
     * @param dto
     * @return
     */
    List<ActivityPublishAllManagementListVO> operateAllPage(ActivityPublishPageDTO dto);

    /***
     * @Description:获取我参与的/管理的/创建的活动数量
     * @Author: zhangzy
     * @Date: 2022/6/6 17:11
     * @param activityMyCountQueryDTO
     *                                {@link ActivityCountBO}
     **/
    ActivityCountBO myCount(ActivityMyCountQueryDTO activityMyCountQueryDTO);

    Integer pageCount(ActivityPublishPageDTO dto);

    List<ActivityPublishPageVO> page(ActivityPublishPageDTO dto);

    /**
     * 获取我报名的
     * 
     * @param build
     * @return
     */
    List<ActivityPublishGroupUserCount> countByUserIdList(ActivityPublishPageDTO build);

    void updateTopIndex(ActivitySetTopIndexDTO activitySetTopIndexDTO);

    Integer planPageCount(ActivityPageDTO dto);

    List<ActivityPlanPageBO> planPage(ActivityPageDTO dto);

    /***
     * @Description:活动数量查询
     * @Author: zhangzy
     * @Date: 2022/8/1 11:59
     * @param request
     *                {@link Integer}
     **/
    Integer activityCountQuery(ActivityCountQueryFeignDTO request);

    /**
     * 查询所有活动信息
     *
     * @return java.util.List<com.dt.activity.center.bo.activity.ActivityBO>
     * @author CLT
     * @date 2022/11/21 15:11
     */
    List<ActivityBO> selectAllActivity();

    /***
     * @Description:在线教研J02数据条数查询
     * @Author: zhangzy
     * @Date: 2023/2/9 15:00
     * @param dto
     *            {@link Integer}
     **/
    Integer countByZxjyPageDTO(ZxjyAdminActivityPageDTO dto);

    /***
     * @Description:在线教研J02数据查询
     * @Author: zhangzy
     * @Date: 2023/2/9 15:00
     * @param dto
     *            {@link List< ZxjyAdminActivityPageDOEx>}
     **/
    List<ZxjyAdminActivityPageDOEx> pageByZxjyPageDTO(ZxjyAdminActivityPageDTO dto);

    /***
     * @Description:在线教研J02数据查询
     * @Author: zhangzy
     * @Date: 2023/2/9 15:33
     * @param scene
     * @param activityIds
     *                    {@link List< ZxjyAdminActivityPageDOEx>}
     **/
    List<ZxjyAdminActivityPageDOEx> listZxjyActivityBySceneAndIds(@Param("scene") String scene,
                                                                  @Param("activityIds") List<Long> activityIds);

    /***
     * @Description:根据学校id集合及品牌 获取学校下去重教师数量
     * @Author: zhangzy
     * @Date: 2023/2/14 12:16
     * @param dto
     *            {@link List< SchoolUserStatisticsDOEx>}
     **/
    List<SchoolUserStatisticsDOEx> statisticsBySchoolIdsAndBrand(SchoolUserStatisticsDTO dto);

    /**
     * 同步单个活动的状态
     * 
     * @param id
     * @param state
     */
    void updateActivityStatusByActivityId(@Param("id") Long id, @Param("state") Integer state);

    /**
     * 查询实际已经开始，但状态是等待开启的活动
     * 
     * @return
     */
    List<Long> selectBeginActivityButState();

    /**
     * 查询实际已经结束，但状态是进行中的活动
     * 
     * @return
     */
    List<Long> selectEndActivityButState();

    /**
     * 更新活动状态为进行中
     * 
     * @param ids
     */
    void updateProcessingStateByIds(@Param("ids") List<Long> ids);

    /**
     * 更新活动状态为已结束
     * 
     * @param ids
     */
    void updateEndStateByIds(@Param("ids") List<Long> ids);

    /***
     * @Description:查询用户最近参加的活动
     * @Author: zhangzy
     * @Date: 2023/2/27 10:22
     * @param activityMyCountQueryDTO
     *                                {@link List< ActivityPageDO>}
     **/
    List<ActivityPageDO> lastJoinListByQueryDTO(ActivityMyCountQueryDTO activityMyCountQueryDTO);

    /***
     * @Description:根据模板id集合 及活动状态查询活动id
     * @Author: zhangzy
     * @Date: 2023/3/29 15:09
     * @param templateIdList
     * @param state
     *                       {@link List< Long>}
     **/
    List<Long> listActivityByTemplateIdListAndState(@Param("templateIdList") List<Long> templateIdList,
                                                    @Param("state") Integer state);

    /**
     * 查询活动列表
     * 
     * @param ids
     * @param start
     * @param page
     * @return
     */
    List<ActivityDO> selectByIds(@Param("ids") List<Long> ids, @Param("offset") Long offset,
                                 @Param("limit") Long limit);

    /***
     * @Description:查询走主库
     * @Author: zhangzy
     * @Date: 2023/8/1 10:27
     * @param
     *
     **/
    Long selectForUpdate();

    /***
     * @Description:
     * @Author: zhangzy
     * @Date: 2024/1/2 11:58
     * @param ids
     * @param userId
     *               {@link List< ActivityPageDO>}
     **/
    List<ActivityPageDO> listByActivityIdsAndUserId(@Param("ids") List<Long> ids, @Param("userId") Long userId);

    /***
     * @Description:参加活动数量及最近参加时间
     * @Author: zhangzy
     * @Date: 2024/1/9 11:40
     * @param activityMyCountQueryDTO
     *                                {@link com.dt.activity.center.feign.server.dto.ActivityCountAndLastResultDTO}
     **/
    ActivityCountAndLastResultDTO countAndLastByUser(ActivityMyCountQueryDTO activityMyCountQueryDTO);
}

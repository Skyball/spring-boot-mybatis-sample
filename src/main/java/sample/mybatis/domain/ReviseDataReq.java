package sample.mybatis.domain;

import javax.persistence.Entity;
import java.util.List;

@Entity
public class ReviseDataReq {

    private List<String> queryCondition:
    private String operator:
    private String checkCode;

    public List<String> getQueryCondition() {
        return queryCondition;
    }

    public void setQueryCondition(List<String> queryCondition) {
        this.queryCondition = queryCondition;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }
}

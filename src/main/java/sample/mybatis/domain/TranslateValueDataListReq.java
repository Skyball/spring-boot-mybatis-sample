package sample.mybatis.domain;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TranslateValueDataListReq {
    private String offset;
    @Id
    private Long id;

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    private String pageSize;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}

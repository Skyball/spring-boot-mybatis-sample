package sample.mybatis.service;

import com.dt.activity.center.bo.activity.*;
import com.dt.activity.center.dto.activity.ActivityDetailDTO;
import com.dt.activity.center.dto.activity.ActivityMyCountQueryDTO;
import com.dt.activity.center.dto.activity.ActivityPageDTO;

import com.dt.activity.center.dto.activity.ZxjyAdminActivityPageDTO;
import com.dt.activity.center.feign.server.dto.*;
import com.dt.activity.center.model.ActivityDO;
import com.dt.activity.center.web.model.activity.*;
import com.dt.activity.center.feign.server.vo.ActivityCountQueryFeignVO;
import com.dt.activity.center.feign.server.dto.ActivityAchievementQueryResultFeignDTO;
import com.dt.activity.center.web.model.activity.statistics.ActivityStatisticsDTO;
import com.dt.framework.core.mvc.Pagination;
import com.dt.framework.core.mvc.WebResult;

import java.util.List;
import java.util.Map;

/**
 * 活动表 (Activity)表服务接口
 *
 * @author zhangzy
 * @since 2021-06-24 12:20:16
 */
public interface ActivityReader {

    /***
     * 根据活动id获取活动信息
     * 
     * @param activityId 活动id
     * @return
     */
    ActivityBO selectByActivityId(Long activityId);

    /***
     * 根据活动缓存获取活动
     * 
     * @param activityDetailDTO
     * @return
     */
    ActivityBO getActivityBOFromRedis(ActivityDetailDTO activityDetailDTO);

    /***
     * @Description:根据条件查询活动详情
     * @Author: zhangzy
     * @Date: 2021/6/25 10:32
     * @param activityDetailDTO
     *                          {@link ActivityBO}
     **/
    ActivityBO selectActivityDetail(ActivityDetailDTO activityDetailDTO);

    /***
     * @Description:根据条件查询活动列表
     * @Author: zhangzy
     * @Date: 2021/6/28 11:31
     * @param activityPageDTO
     *                        {@link Pagination< ActivityPageBO>}
     **/
    Pagination<ActivityPageBO> page(ActivityPageDTO activityPageDTO);

    /***
     * @Description:获取该活动的模板code
     * @Author: zhangzy
     * @Date: 2021/8/3 10:20
     * @param activityId
     *                   {@link Integer}
     **/
    Integer getTemplateCodeByActivityId(Long activityId);

    /***
     * @Description:通过id集合 获取活动内容列表
     * @Author: zhangzy
     * @Date: 2021/10/14 9:31
     * @param ids
     *            {@link List< ActivityPageBO>}
     **/
    List<ActivityPageBO> listByActivityIds(List<Long> ids);

    /***
     * @Description:该用户常用的标签列表
     * @Author: zhangzy
     * @Date: 2021/10/14 19:51
     * @param userId
     *               {@link WebResult< List< String>>}
     **/
    List<String> userTypeTags(Long userId);

    /***
     * @Description:获取我的活动数量(我参与的 我管理的 我创建的)
     * @Author: zhangzy
     * @Date: 2022/6/6 16:44
     * @param activityMyCountQueryDTO
     *                                {@link ActivityCountBO}
     **/
    ActivityCountBO myCount(ActivityMyCountQueryDTO activityMyCountQueryDTO);

    /**
     * 后台活动列表
     *
     * @param dto
     * @return
     */
    Pagination<ActivityManagementListVO> operatePlanAllPage(ActivityPageDTO dto);

    /**
     * 前台台活动列表
     *
     * @param dto
     * @return
     */
    Pagination<ActivityPlanListVO> onlinePlanPage(ActivityPageDTO dto);

    /***
     * @Description:根据场景获取主题及组织单位数据
     * @Author: zhangzy
     * @Date: 2022/7/19 16:58
     * @param scene
     *              {@link TemplateThemeAndOrgVO}
     **/
    TemplateThemeAndOrgVO themeAndOrg(String scene);

    /**
     * 活动基础查询
     *
     * @param dto
     * @return
     */
    Pagination<ActivityBaseListVO> basePage(ActivityPageDTO dto);

    /***
     * @Description:活动数量查询
     * @Author: zhangzy
     * @Date: 2022/8/1 11:58
     * @param request
     *                {@link ActivityCountQueryFeignVO}
     **/
    ActivityCountQueryFeignVO activityCountQuery(ActivityCountQueryFeignDTO request);

    /**
     * 获取活动id和场景
     *
     * @return java.util.List<com.dt.activity.center.bo.activity.ActivityBO>
     * @author CLT
     * @date 2022/11/21 15:08
     */
    List<ActivityBO> selectActivityByDTO(ActivityCompletionUserFeignDTO build);

    /***
     * @Description:根据活动集合获取活动学科学段
     * @Author: zhangzy
     * @Date: 2022/11/30 14:22
     * @param activityIds
     *                    {@link List< ActivityStageSubjectDTO>}
     **/
    List<ActivityStageSubjectDTO> getActivityStageSubject(List<Long> activityIds);

    /***
     * @Description:在线教研J02业务获取活动列表
     * @Author: zhangzy
     * @Date: 2023/2/9 14:24
     * @param dto
     *            {@link Pagination< ActivityBaseListVO>}
     **/
    Pagination<ZxjyAdminActivityPageBO> zxjyPage(ZxjyAdminActivityPageDTO dto);

    /***
     * @Description:在线教研 根据活动id集合 及场景获取数据
     * @Author: zhangzy
     * @Date: 2023/2/9 15:30
     * @param scene
     * @param activityIds
     *                    {@link List< ZxjyAdminActivityPageVO>}
     **/
    List<ZxjyAdminActivityPageBO> getZxjyActivityListByActivityIds(String scene, List<Long> activityIds);

    /***
     * @Description:查询活动基础信息单表数据
     * @Author: zhangzy
     * @Date: 2023/2/14 10:21
     * @param id
     *           {@link ActivityDO}
     **/
    ActivityDO selectDOByActivityId(Long id);

    /***
     * @Description:根据学校id集合及品牌 获取学校下去重教师数量
     * @Author: zhangzy
     * @Date: 2023/2/14 12:12
     * @param dto
     *            {@link List< SchoolUserStatisticsFeignBO>}
     **/
    List<SchoolUserStatisticsFeignBO> statisticsBySchoolIdsAndBrand(SchoolUserStatisticsDTO dto);

    /***
     * @Description:根据场景获取教研字典
     * @Author: zhangzy
     * @Date: 2023/2/16 11:07
     * @param scene
     *              {@link Object}
     **/
    ActivityResearchDictVO getActivityResearchDict(String scene);

    /***
     * @Description:获取专家 主讲人 主备人 信息
     * @Author: zhangzy
     * @Date: 2023/2/24 9:33
     * @param activityBOPagination
     *
     **/
    void fillExpertList(Pagination<ZxjyAdminActivityPageBO> activityBOPagination);

    /***
     * @Description:通过活动id或访问码查询活动基础数据 有缓存
     * @Author: zhangzy
     * @Date: 2023/3/2 11:42
     * @param activityDetailDTO
     *                          {@link ActivityBO}
     **/
    ActivityBO selectActivityBaseDetail(ActivityDetailDTO activityDetailDTO);

    /***
     * @Description:用户最近参加的活动
     * @Author: zhangzy
     * @Date: 2023/2/27 10:17
     * @param activityMyCountQueryDTO
     *                                {@link List< ActivityStatisticsDTO>}
     **/
    List<ActivityStatisticsDTO> lastJoinListByQueryDTO(ActivityMyCountQueryDTO activityMyCountQueryDTO);

    /***
     * @Description:查询走主库
     * @Author: zhangzy
     * @Date: 2023/8/1 10:27
     * @param
     *
     **/
    void selectForUpdate();

    /***
     * @Description:根据工具id获取活动集合
     * @Author: zhangzy
     * @Date: 2023/9/15 9:33
     * @param ids
     *            {@link List< ActivityPageBO>}
     **/
    Map<Long, ActivityPageBO> mapActivityByToolIds(List<Long> ids);

    /***
     * @Description:
     * @Author: zhangzy
     * @Date: 2023/3/29 15:08
     * @param templateIdlist
     * @param state
     *                       {@link List< Long>}
     **/
    List<Long> listActivityByTemplateIdListAndState(List<Long> templateIdList, Integer state);

    /***
     * @Description:通过活动id集合 获取活动内容列表(带参与状态)
     * @Author: zhangzy
     * @Date: 2024/1/2 11:56
     * @param ids
     * @param userId
     *               {@link List< ActivityPageBO>}
     **/
    List<ActivityPageBO> listByActivityIdsAndUserId(List<Long> ids, Long userId);

    /***
     * @Description:获取用户参加活动次数及上次参加时间
     * @Author: zhangzy
     * @Date: 2024/1/9 11:39
     * @param activityMyCountQueryDTO
     *                                {@link ActivityCountAndLastResultDTO}
     **/
    ActivityCountAndLastResultDTO countAndLastByUser(ActivityMyCountQueryDTO activityMyCountQueryDTO);
}

package sample.mybatis.web;

import sample.mybatis.domain.ReviseDataReq;
import sample.mybatis.domain.TranslateValueDataListReq;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sample.mybatis.service.MapDataServiceImpl;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Jdbc Attack @2023.04
 */
@RestController
@RequestMapping("/mapData")
public class MapDataController {

    @Resource
    private MapDataServiceImpl mapDataServiceImpl;

    @RequestMapping("/listTranslateValueData")
    public Response<List<Map<String, Object>>> listTranslateValueData(String gateWayName, TranslateValueDataListReq req) {
        return Response.buildSuccessInfo(mapDataServiceImpl.listTranslateValueData(gateWayName, req));
    }
    @RequestMapping("/reviseProductionEnvironmentData")
    public Response<List<String>> reviseProductionEnvironmentData(ReviseDataReq reviseDataReq) {
        Config application = ConfigService.getConfig("application");
        Boolean property = application.getBooleanProperty(ReviseDataContext.REVISE_DATA_ENABLED, false);
        if (!property) {
            return Response.buildSuccessResponseWithInfo(Collections.singletonList("未开放的接口权限，请联系管理员!"));
        }
        LOGGER.info("调用【reviseProductionEnvironmentData】获取权限, reviseDataReq: {}.", JsonTools.defaultMapper().toJson(reviseDataReq));
        try {
            return mapDataServiceImpl.reviseProductionEnvironmentData(reviseDataReq, true);
        } catch (Exception e) {
            if (e instanceof BblBusinessException) {
                return Response.buildSuccessResponseWithInfo(Collections.singletonList(((BblBusinessException) e).getMsg()));
            }
            return Response.buildSuccessResponseWithInfo(Collections.singletonList(e.getMessage()));
        }
    }

}
